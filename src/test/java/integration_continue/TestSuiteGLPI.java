package integration_continue;

import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import junitparams.JUnitParamsRunner;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSuiteGLPI {

	public static ChromeDriver driver;

	@BeforeClass
	public static void setupClass() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://demo.glpi-project.org/index.php");
	}

	@Test
	public void a_testConnexion() {
		assertTrue(driver.getTitle().contains("Authentication"));
		driver.findElement(By.id("login_name")).sendKeys("admin");
		driver.findElement(By.id("login_password")).sendKeys("admin");
		new Select(driver.findElement(By.name("language"))).selectByVisibleText("Fran�ais");
		driver.findElement(By.name("submit")).click();
		assertTrue(driver.findElement(By.tagName("body")).getText().contains("Accueil"));
		assertTrue(driver.findElement(By.tagName("body")).getText().contains("Admin"));
	}

	@Test
	public void b_testAjouterOrdi() {
		driver.findElement(By.linkText("Parc")).click();
		driver.findElement(By.linkText("Ordinateurs")).click();
		driver.findElement(By.xpath("//a[@title='Ajouter']")).click();
		driver.findElement(By.linkText("Gabarit vide")).click();
		String varOrdinateur = getRandomReference();
		String varSerie = getRandomReference();
		/* nom */
		driver.findElement(By.name("name")).sendKeys(varOrdinateur);
		/* numero serie */
		driver.findElement(By.name("serial")).sendKeys(varSerie);
		/* Commentaire */
		driver.findElement(By.id("comment")).sendKeys("Mon commentaire");
		// bonus//		
		/*-> Reponsable technique */
		driver.findElement(By.xpath("//select[@name='users_id_tech']/parent::td")).click();
		driver.findElement(By.xpath("//li[contains(.,'test')]")).click();
		/*-> Domaine */
		driver.findElement(By.xpath("//span[contains(@aria-labelledby,'select2-dropdown_domains_id')]")).click();
		driver.findElement(By.xpath("//span[text()='CAMPUS']")).click();
		/* ajout */
		driver.findElement(By.name("add")).click();
		/* Verification cr�ation avec recherche ordi ajouter */
		driver.findElement(By.name("globalsearch")).sendKeys(varOrdinateur);
		driver.findElement(By.name("globalsearchglass")).click();
		assertTrue(driver.findElement(By.tagName("body")).getText().contains(varSerie));
		assertTrue(driver.findElement(By.tagName("body")).getText().contains(varOrdinateur));
		driver.findElement(By.linkText(varOrdinateur)).click();
	}

	@Test
	public void c_testDeconnexion() {
		assertTrue(driver.findElementByCssSelector("a.fa.fa-sign-out").isDisplayed());
		driver.findElementByCssSelector("a.fa.fa-sign-out").click();
		assertTrue(driver.getTitle().contains("Authentication"));
	}

	@AfterClass
	public static void tearDownClass() {
		driver.quit();
	}

	public String getRandomReference() {
		String ref = "";
		String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWYZ";
		int stringLength = 4;
		for (int i = 0; i < stringLength; i++) {
			int rnum = (int) Math.floor(Math.random() * allowedChars.length());
			ref += allowedChars.substring(rnum, rnum + 1);
		}
		ref += "-";
		for (int i = 0; i < stringLength; i++) {
			int rnum = (int) Math.floor(Math.random() * 9);
			ref += rnum;
		}
		return ref;
	}

}
